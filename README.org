# Ansible Collection - zacnix.dockerserver

* Collection - zacnix.DockerServer
A collection to install docker and docker-compose as well as push up directories to the server.

* Testing
Testing should be fairly straight forward. You can tangle the files by running emacs,
~emacs --batch --eval "(require 'org)" --eval '(org-babel-tangle-file "./README.org")'~
Then setup your inventory file and set the file paths for the server_files variable.

#+BEGIN_SRC yaml :tangle tests/vars
server_files:
  - ""
#+END_SRC

#+BEGIN_SRC yaml :tangle tests/play.yml
- hosts: dckserv
  remote_user: "root"
  collections:
    - zacnix.dockerserver
  tasks:
    - name: Run the docker installer
      include_vars:
        file: vars
      import_role:
        name: dckserver
#+END_SRC
